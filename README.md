GCode Viewer
============

A web-based 3D viewer for [GCode](http://en.wikipedia.org/wiki/G-code) files.
Basic mouse controls:
- Right Click to move side to side
- Left Click to rotate
- Scrollwheel to zoom

Built using WebGL and [three.js](https://github.com/mrdoob/three.js/).

![Screenshot](img.jpg)

Use it
------

* https://fablabbcn-projects.gitlab.io/cnc-machines/g-code-visualizer/

Source base code from -[Joe Walnes](https://github.com/joewalnes) reworked by Fab Lab Barcelon Future Learning Unit-[EduardoChamorro](https://github.com/EDUARDOCHAMORRO/) 24/04/2020
Open sourced under MIT license.


[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/joewalnes/gcode-viewer/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

